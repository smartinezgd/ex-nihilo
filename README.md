# Ex-Nihilo Cardmaker #

Ex-Nihilo Cardmaker est un outil de création de cartes pour le jeu [Ex-Nihilo de Romaric Briand](https://romaricbriand.fr/ex-nihilo/). Il permet de générer des planches de cartes prêtes à être imprimées à partir d'un fichier tableur au format csv où les cartes sont décrites.

## Crédits et licence / Credits and license ##

Ex-Nihilo est un logiciel libre publié sous licence GPL2. Il est foruni avec un modèle de carte ainsi que la police d'écriture *Libre Baskeville* conçue par Pablo Impallari, distribuée sous licence *SIL Open Font* dont le texte complet est lisible dans le fichier *FONT_LICENSE* du dossier `cardmaker/ressources`. La police est téléchargeable indépendament sur le [github du concepteur](https://github.com/impallari/Libre-Baskerville) 

Ex-Nihilo is a piece of software distributed under GPL2 license. It is provided with a card template and the *Libre Baskerville* font designed by Pablo Impallari and distributed under the *SIL Open Font* which full text can be read in file *FONT_LICENSE* of folder `cardmaker/ressources`. The font can be separately downloaded on the [designer's github](https://github.com/impallari/Libre-Baskerville)

## Installation ##

### Windows ###

Un exécutable prêt à lancer pour Windows 10 est téléchargeable sur la [page de téléchargement](https://bitbucket.org/smartinezgd/ex-nihilo/downloads/)
Cet exécutable est probablement compatible avec des versions antérieures de Windows, mais cela n'a pas été testé. 

Téléchargez `cardmaker.zip` et décompressez-le à l'endroit de votre choix. Un dossier `cardmaker` sera créé contenant les fichiers du logiciel ansi que l'exécutable `cardmaker.exe`.

Téléchargez également le modèle de fichier tableur `deck.ods` sur la page de téléchargement.

### Windows, créer un nouvel exécutable ###

L'exécutable disponible sur la page de téléchargement devrait suffire. Si ce n'est pas le cas, voilà quelques informations pour en générer un nouveau. 

Le *freeze* des applications Python sous Windows déclenche généralement un faux positif de la plupart des antivirus. Ex-Nihilo n'échappe pas à cette généralité et seul l'outil *cxfreeze* semble éviter ce problème.

Le fichier setup_cxfreeze est fourni dans le dépôt pour permettre de regénérer des éxecutables Windows en cas de besoin. Reportez-vous à la [documentation de cxfreeze](https://cx-freeze.readthedocs.io/en/latest/) pour plus d'instructions.

### Linux ###

Ex-Nihilo utilise Python 3 et dépend des bibliothèques [Pillow](https://pypi.org/project/Pillow/) et [PyQT5](https://pypi.org/project/PyQt5/) pour manipuler les images et afficher son interface graphique. Installer l'interpréteur python et les bibliothèques de la manière de votre choix.

Exécutez ensuite `python setup.py install --prefix=<chemin de votre choix>` pour installer le logiciel.

Téléchargez ensuite le modèle de fichier tableur `deck.ods` sur la [page de téléchargement](https://bitbucket.org/smartinezgd/ex-nihilo/downloads/).

## Utilisation ##

### Éditer le fichier tableur et générer un fichier .csv ###

1. Ouvrir fichier "deck.ods" et y entrer ses cartes à raison d'une
   carte par ligne, en spécifiant les noms, temps et textes d'ambiance
   dans les colones appropriées.

2. Enregistrer le fichier au format ".csv" en faisant "enregistrer
   sous" et en sélectionnant le format "Text CSV". Il est possible
   qu'une fenêtre apparaisse pour avertir qu'enregistrer au format CSV
   fera perdre des informations. Continuer tout de même et choisir les
   caractères ; et " respectivement comme délimiteurs de champs et
   comme délimiteur de chaîne de caractères.

### Générer les planches de carte à imprimer ###

3. Lancer cardmaker.exe et sélectionner le logo du deck (facultatif)
   ainsi que l'image de fond des cartes (facultatif). Sélectionner le
   fichier CSV généré précédemment, puis sélectionner le
   dossier où enregistrer les cartes.

4. Vous pouvez également choisir des icônes personnalisées pour les
   temps. Si vous ne changez rien, les icônes par défaut seront
   utilisées.

5. Cliquer sur "Créer le Deck" et patienter jusqu'à qu'une fenêtre
   "pop-up" indique que les cartes ont été créées.

6. Les planches de cartes sont enregistrées dans le dossier
   sélectionné à l'étape 3, au format PNG avec une résolution de 300
   pixels par pouces.



