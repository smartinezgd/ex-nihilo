from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
build_options = {'packages': [], 'excludes': [], 'include_files':[("cardmaker\\resources\\cardback.png","resources\\cardback.png"),("cardmaker\\resources\\encre.png","resources\\encre.png"),("cardmaker\\resources\\sang.png","resources\\sang.png"),("cardmaker\\resources\\seve.png","resources\\seve.png"),("cardmaker\\resources\\larme.png","resources\\larme.png"),("cardmaker\\resources\\title_font.ttf","resources\\title_font.ttf"),("cardmaker\\resources\\text_font.ttf","resources\\text_font.ttf"),("cardmaker\\resources\\icon.jpg","resources\\icon.jpg"),("cardmaker\\resources\\back.jpg","resources\\back.jpg"),("cardmaker\\resources\\FONT_LICENSE.txt","resources\\FONT_LICENSE.txt")]}

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('bin\\cardmaker', base=base, target_name = 'cardmaker')
]

setup(name='CardMaker',
      version = '1.0',
      description = 'Editeur de cartes Ex-Nihilo',
      options = {'build_exe': build_options},
      executables = executables)
