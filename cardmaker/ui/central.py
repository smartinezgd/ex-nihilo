# -*- coding: utf-8 -*-

#    This file is part of CardMaker
#    Copyright (C) 2021 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QGridLayout, QLabel, QPushButton, QFileDialog, QSizePolicy, qApp
from PyQt5.QtGui import QFont,QIcon
from PyQt5.QtCore import Qt,QSize
from cardmaker.resources import get_resource_path 
import os
import imghdr


class ImageWidget(QWidget):
    def __init__(self,parent,xsize,ysize,blank,picname,setter):
        super().__init__(parent)
        self.central_view = parent
        self.button = QPushButton(self)
        self.xsize = xsize
        self.ysize = ysize
        self.picname = picname
        self.setter = setter
        self.blank = os.path.join(get_resource_path(),blank)
        self.button.setFixedHeight(self.xsize)
        self.button.setFixedWidth(self.ysize)
        self.button.setIcon(QIcon(self.blank))
        self.button.setIconSize(QSize(xsize-10,ysize-10))
        self.button.clicked.connect(self.edit)
        self.layout = QHBoxLayout()
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def edit(self):
        img = QFileDialog.getOpenFileName(self, "Séléctionner "+self.picname, '.',filter=('Images (*.jpeg *.jpg *.png)'))[0]
        if img:
            try:
                if imghdr.what(img) in ["jpeg","png"]:
                    self.button.setIcon(QIcon(img))
                    self.button.setIconSize(QSize(self.xsize-10,self.ysize-10))
                    self.setter(img)
            except IOError:
                self.clear()

    def clear(self):
        self.button.setIcon(QIcon(self.blank))
        self.button.setIconSize(QSize(self.xsize-10,self.ysize-10))


class CSVView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.central_view = parent
        self.layout = QVBoxLayout()
        self.csvbutton = QPushButton(self)
        self.csvbutton.setText("Selectionner le fichier .csv du deck")
        self.csvbutton.clicked.connect(self.set_csv)
        self.csvbutton.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.csvbutton.sizePolicy.setHorizontalStretch(1)
        self.layout.addWidget(self.csvbutton)
        self.setLayout(self.layout)


    def set_csv(self):
        d = QFileDialog.getOpenFileName(self, 'Selectionnez le fichier .csv du deck', '.',filter=('CSV (*.csv)'))[0]
        if d:
            self.central_view.app.csv = d
            t = os.path.basename(d)
            if len(t) > 20:
                self.csvbutton.setText(t[0:17]+"...")
            else:
                self.csvbutton.setText(t)

class WorkDirView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.central_view = parent
        self.layout = QVBoxLayout()
        self.button = QPushButton(self)
        self.button.setText("Selectionner le dossier de travail")
        self.button.clicked.connect(self.set_workdir)
        self.button.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.button.sizePolicy.setHorizontalStretch(1)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def set_workdir(self):
        d = QFileDialog.getExistingDirectory(self, 'Selectionnez le dossier où enregistrer les cartes', '.')
        if d:
            self.central_view.app.workdir = d
            t = os.path.basename(d)
            if len(t) > 20:
                self.button.setText(t[0:17]+"...")
            else:
                self.button.setText(t)

class GoButton(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.central_view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.button = QPushButton(self)
        self.button.setText("Créer le deck")
        self.button.clicked.connect(self.go)
        self.button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.layout.addWidget(self.button)
        self.label = QLabel(self)
        self.label.setFont(self.bold)
        self.label.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.label.sizePolicy.setHorizontalStretch(1)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setText("En attente")
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)


        
    def go(self):
        self.label.setText("Création du deck en cours...")
        qApp.processEvents()
        self.central_view.mainwin.create_deck()
        self.label.setText("En attente")
                

class TempsWidget(ImageWidget):
    def __init__(self,parent,default,temps,setter):
        super().__init__(parent,60,60,"","l'icône du temps "+temps,setter)
        self.blank = default
        self.button.setIcon(QIcon(self.blank))
        self.button.setIconSize(QSize(self.xsize-10,self.ysize-10))


class TempsView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.layout = QGridLayout()
        self.app = parent.app
        self.seve_view = TempsWidget(self,self.app.seve_icon,"de sève",self.set_seve)
        self.larme_view = TempsWidget(self,self.app.larme_icon,"de larme",self.set_larme)
        self.sang_view = TempsWidget(self,self.app.sang_icon,"de sang",self.set_sang)
        self.encre_view = TempsWidget(self,self.app.encre_icon,"d'encre",self.set_encre)
        self.bold = QFont()
        self.bold.setBold(True)
        self.seve_lbl = QLabel(self)
        self.seve_lbl.setText("Sève")
        self.larme_lbl = QLabel(self)
        self.larme_lbl.setText("Larme")
        self.sang_lbl = QLabel(self)
        self.sang_lbl.setText("Sang")
        self.encre_lbl = QLabel(self)
        self.encre_lbl.setText("Encre")
        self.label = QLabel(self)
        self.label.setFont(self.bold)
        self.label.setText("Icônes des temps")
        self.layout.addWidget(self.label,0,0,1,2,Qt.AlignCenter)
        self.layout.addWidget(self.seve_lbl,1,0,Qt.AlignCenter)
        self.layout.addWidget(self.seve_view,2,0,Qt.AlignCenter)
        self.layout.addWidget(self.encre_lbl,1,1,Qt.AlignCenter)
        self.layout.addWidget(self.encre_view,2,1,Qt.AlignCenter)
        self.layout.addWidget(self.larme_lbl,3,0,Qt.AlignCenter)
        self.layout.addWidget(self.larme_view,4,0,Qt.AlignCenter)
        self.layout.addWidget(self.sang_lbl,3,1,Qt.AlignCenter)
        self.layout.addWidget(self.sang_view,4,1,Qt.AlignCenter)
        self.setLayout(self.layout)
        
    def set_seve(self,icon):
        self.app.seve_icon = icon
    def set_larme(self,icon):
        self.app.larme_icon = icon
    def set_sang(self,icon):
        self.app.sang_icon = icon
    def set_encre(self,icon):
        self.app.encre_icon = icon


        
class CentralView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.mainwin = parent
        self.layout = QGridLayout()
        self.icon_view = ImageWidget(self,130,130,"icon","l'icône du deck",self.set_icon)
        self.layout.addWidget(self.icon_view,0,0,2,1,Qt.AlignTop)
        self.tempsview = TempsView(self)
        self.layout.addWidget(self.tempsview,2,0,2,1,Qt.AlignTop)
        self.csvview = CSVView(self)
        self.layout.addWidget(self.csvview,4,0,Qt.AlignCenter)
        self.workview = WorkDirView(self)
        self.layout.addWidget(self.workview,4,1,Qt.AlignCenter)
        self.backview = ImageWidget(self,360,230,"back","l'image de fond du deck",self.set_back)
        self.layout.addWidget(self.backview,0,1,4,1,Qt.AlignCenter)
        self.gobutton = GoButton(self)
        self.layout.addWidget(self.gobutton,5,0,1,2,Qt.AlignTop)
        self.setLayout(self.layout)

        
    def set_icon(self,icon):
        self.app.cardicon = icon
    def set_back(self,back):
        self.app.backpic = back






