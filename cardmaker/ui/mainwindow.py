# -*- coding: utf-8 -*-

#    This file is part of CardMaker
#    Copyright (C) 2021 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, qApp
from cardmaker.ui.widgets import gen_info_dialog,gen_yes_no_dialog
from cardmaker.ui.central import CentralView
import os



class MainWindow(QMainWindow):
    def __init__(self,application):
        super().__init__()
        self.title = "Ex Nihilo Card Maker"
        self.app = application
        self.left = 30
        self.top = 30
        self.width = 510
        self.height = 540
        self.setWindowTitle(self.title)
        self.setFixedSize(self.width,self.height)
        self.central_view = CentralView(self)
        self.setCentralWidget(self.central_view)
        self.show()

 
    def create_deck(self):
        res = self.app.check_config()
        if res[1] != "":
            if res[0]:
                gen_info_dialog(self,"Attention!",res[1])
            else:
                gen_info_dialog(self,"Erreur!",res[1])
        if res[0]:
            self.app.update_card_template()
            r= self.app.check_csv_file()
            if r[0]:
                gen_info_dialog(self,"Erreur!",r[1])
            else:
                if r[1] != "":
                    gen_info_dialog(self,"Attention!",r[1])
                qApp.processEvents()
                self.app.create_deck()
                self.app.gen_planks()
                gen_info_dialog(self,"Terminé!","Les planches de cartes ont été enregistrées dans le dossier de travail.")
