# -*- coding: utf-8 -*-

#    This file is part of CardMaker
#    Copyright (C) 2021 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os

def get_resource_path():
    this_dir,this_file = os.path.split(__file__)
    return os.path.join(this_dir,"resources")

#CX_Freeze version
#import sys
#def get_resource_path():
#    return os.path.join(os.path.dirname(os.path.abspath(sys.executable)),"resources")
