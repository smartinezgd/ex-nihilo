# -*- coding: utf-8 -*-

#    This file is part of CardMaker
#    Copyright (C) 2021 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import csv
from PIL import Image, ImageFont, ImageDraw
from cardmaker.resources import get_resource_path

cardback = os.path.join(get_resource_path(),"cardback.png")
font = os.path.join(get_resource_path(),"title_font.ttf")
font2 = os.path.join(get_resource_path(),"text_font.ttf")
sangp = os.path.join(get_resource_path(),"sang.png")
encrep = os.path.join(get_resource_path(),"encre.png")
sevep = os.path.join(get_resource_path(),"seve.png")
larmep = os.path.join(get_resource_path(),"larme.png")

class Application(object):
    def __init__(self):
        self.csv = None
        self.backpic = None
        self.cardicon = None
        self.workdir = "."
        self.template=None
        self.cards = []
        self.title_font=ImageFont.truetype(font,50)  #46
        self.smaller_title_font=ImageFont.truetype(font,45) #38
        self.text_font=ImageFont.truetype(font2,35) 
        self.sang = None
        self.encre = None
        self.seve = None
        self.larme = None
        self.sang_icon = sangp
        self.encre_icon = encrep
        self.seve_icon = sevep
        self.larme_icon = larmep

    def open_times(self):
        self.sang = Image.open(self.sang_icon)
        width,height = self.sang.size
        if width > 160 or height > 160:
            self.sang.thumbnail((160,160))
            width,height = self.sang.size
        self.sang_xdelta=int(width/2)
        self.sang_ydelta=int(height/2)
        self.encre = Image.open(self.encre_icon)
        width,height = self.encre.size
        if width > 160 or height > 160:
            self.encre.thumbnail((160,160))
            width,height = self.encre.size
        self.encre_xdelta=int(width/2)
        self.encre_ydelta=int(height/2)
        self.seve = Image.open(self.seve_icon)
        width,height = self.seve.size
        if width > 160 or height > 160:
            self.seve.thumbnail((160,160))
            width,height = self.seve.size
        self.seve_xdelta=int(width/2)
        self.seve_ydelta=int(height/2)
        self.larme = Image.open(self.larme_icon)
        width,height = self.larme.size
        if width > 160 or height > 160:
            self.larme.thumbnail((160,160))
            width,height = self.larme.size
        self.larme_xdelta=int(width/2)
        self.larme_ydelta=int(height/2)
        
    def close_times(self):
        self.sang.close()
        self.encre.close()
        self.seve.close()
        self.larme.close()
        
    def check_readable_file(self,path):
        if not os.path.isfile(path):
            return 1
        else:
            r = True
            try:
                f = open(path,"r")
                f.close()
            except IOError:
                r = False
            if not r:
                return 2
        return 0
        
    def check_config(self):
        #Check if workdir exists and is writeable
        #If not, raise error : we cannot continue
        if not os.access(self.workdir,os.W_OK):
            msg = "Le dossier de travail n'existe pas ou l'ecriture n'y est pas autorisée.\nIl est impossible de continuer."
            return (False,msg)
        #Check if CSV exists and is readable
        #If not, raise error : we cannot continue
        if self.csv == None:
            msg = "Aucun fichier CSV n'a été indiqué.\nIl est impossible de continuer."
            return (False,msg)
        r = self.check_readable_file(self.csv)
        if r == 1:
            msg = "Le fichier CSV du deck n'existe pas.\nIl est impossible de continuer."
            return (False,msg)
        elif r == 2:
            msg = "Le fichier CSV du deck ne peut être lu.\nIl est impossible de continuer."
            return (False,msg)

        msg=""
        #If backpic is not None, check if it exists and if it has the good resolution
        #If not, raise warning and set backpic to a white picture
        if self.backpic != None:
            r = self.check_readable_file(self.backpic)
            if r == 1:
                msg+="L'image de fond n'existe pas. Un fond blanc sera utilisé.\n"
                self.backpic = None
            elif r == 2:
                msg+="L'image de fond ne peut être lue. Un fond blanc sera utilisé.\n"
                self.backpic = None
            else:
                img = Image.open(self.backpic)
                width,height = img.size
                if width < 744 or height < 1039:
                    msg+="L'image de fond est trop petite. Un fond blanc sera utilisé.\n"
                    self.backpic = None
        #If icon is not None, check if it exists and if it has the good resolution
        #If not, raise warning and set icon to None : No icon will be added.
        if self.cardicon != None:
            r = self.check_readable_file(self.cardicon)
            if r == 1:
                msg+="L'icône n'existe pas. Aucune icône ne sera ajoutée.\n"
                self.cardicon = None
            elif r == 2:
                msg+="L'icône ne peut être lue. Aucune icône ne sera ajoutée.\n"
                self.cardicon = None
        #Check if times icons are the correct size (i.e. 160x160)
        r = self.check_readable_file(self.sang_icon)
        if r == 1:
            msg+="L'icône du temps de sang n'existe pas. L'icône par défaut sera utilisée à la place.\n"
            self.sang_icon = sangp
        elif r == 2:
            msg+="L'icône du temps de sang ne peut être lue. L'icône par défaut sera utilisée à la place.\n"
            self.sang_icon = sangp
        r = self.check_readable_file(self.seve_icon)
        if r == 1:
            msg+="L'icône du temps de sève n'existe pas. L'icône par défaut sera utilisée à la place.\n"
            self.seve_icon = sevep
        elif r == 2:
            msg+="L'icône du temps de sève ne peut être lue. L'icône par défaut sera utilisée à la place.\n"
            self.seve_icon = sevep
        r = self.check_readable_file(self.larme_icon)
        if r == 1:
            msg+="L'icône du temps de larme n'existe pas. L'icône par défaut sera utilisée à la place.\n"
            self.larme_icon = larmep
        elif r == 2:
            msg+="L'icône du temps de larme ne peut être lue. L'icône par défaut sera utilisée à la place.\n"
            self.larme_icon = larmep
        r = self.check_readable_file(self.encre_icon)
        if r == 1:
            msg+="L'icône du temps d'encre n'existe pas. L'icône par défaut sera utilisée à la place.\n"
            self.encre_icon = encrep
        elif r == 2:
            msg+="L'icône du temps d'encre ne peut être lue. L'icône par défaut sera utilisée à la place.\n"
            self.encre_icon = encrep
        return (True,msg)


    def update_card_template(self):
        cback = Image.open(cardback)
        self.template = Image.new("RGB",(744,1039),(255,255,255))
        if self.backpic != None:
            backimg = Image.open(self.backpic).convert("RGBA")
            self.template.paste(backimg,(0,0))
        self.template.paste(cback,(0,0),cback)
        cback.close()
        if self.cardicon != None:
            iconimg = Image.open(self.cardicon).convert("RGBA")
            width,height = iconimg.size
            if width > 150 or height > 150:
                iconimg.thumbnail((150,150))
                width,height = iconimg.size
            xdelta=int(width/2)
            ydelta=int(height/2)
            self.template.paste(iconimg,(140-xdelta,880-ydelta),iconimg)
            iconimg.close()
            
    def check_csv_file(self):
        csvf = open(self.csv,"r")
        reader = csv.reader(csvf,delimiter=";",quotechar='"')
        i=0
        msg=""
        error=False
        noTitle=False
        noText=False
        less36=False
        try:
            for row in reader:
                if len(row)<6:
                    msg = "Le fichier CSV n'a pas le bon format.\nIl est impossible de continuer."
                    error=True
                    break
                if i == 0 and (row[0].strip() != "Nom"\
                    or row[1].strip() != "Sève"\
                    or row[2].strip() != "Encre"\
                    or row[3].strip() != "Larme"\
                    or row[4].strip() != "Sang"\
                    or row[5].strip() != "Texte"):
                    msg = "Les noms des colones du fichier CSV sont différentes des noms attendus.\nLa génération des cartes pourrait rencontrer des problèmes.\nIl est impossible de continuer."
                    error=True
                    break
                if i>=1:
                    if row[0].strip() == "":
                        noTitle = True
                    if row[5].strip() == "":
                        noText = True
                    if not (row[1].strip().isdecimal() or row[1].strip() == "") \
                       or not (row[2].strip().isdecimal() or row[2].strip() == "")\
                       or not (row[3].strip().isdecimal() or row[3].strip() == "")\
                       or not (row[4].strip().isdecimal() or row[4].strip() == ""):
                        msg="Les temps de certaines cartes ne sont pas correctement indiqués.\nIl est impossible de continuer."
                        error=True
                        break
                i+=1
                if i>=37:
                    break
        except UnicodeDecodeError:
            error=True
            msg="Impossible de lire le fichier CSV.\nIl est impossible de continuer."
        finally:
            csvf.close()
        if msg=="":
            if i<37:
                less36=True
            if (noTitle and noText) or (noText and less36) or (noText and less36):
                msg = "Plusieurs problèmes ont été constatés dans le fichier CSV:\n"
            if noTitle:
                msg+="Certaines cartes n'ont pas de titre.\n"
            if noText:
                msg+="Certaines cartes n'ont pas de texte d'ambiance.\n"
            if less36:
                msg+="Le Deck contient moins de 36 cartes."
        return (error,msg.strip()) 

    def parse_csv(self):
        csvf = open(self.csv,"r")
        reader = csv.reader(csvf,delimiter=";",quotechar='"')
        i=0
        for row in reader:
            if i>=1:
                c = {}
                emptyCard=True
                if row[0].strip() == "":
                    c["title"] = "Carte Sans Titre"
                else:
                    c["title"] = row[0].strip()
                    emptyCard=False
                if row[5].strip() == "":
                    c["text"] = "Cette carte n'a pas de texte d'ambiance." 
                else:
                    c["text"] = row[5].strip()
                    emptyCard=False
                temps = {}
                total_temps = 0
                if row[1].strip() != "":
                    temps["seve"] = int(row[1].strip())
                    total_temps+= temps["seve"]
                if row[2].strip() != "":
                    temps["encre"] = int(row[2].strip())
                    total_temps+= temps["encre"]
                if row[3].strip() != "":
                    temps["larme"] = int(row[3].strip())
                    total_temps+= temps["larme"]
                if row[4].strip() != "":
                    temps["sang"] = int(row[4].strip())
                    total_temps+= temps["sang"]
                if total_temps > 0:
                    emptyCard=False
                if total_temps > 3 or total_temps < 1:
                    c["temps"] = {}
                    if row[5].strip() == "":
                        c["text"]+= " .N. Les temps de cette carte son illégaux."
                    else:
                        c["text"] = "Les temps de cette carte son illégaux."
                else:
                    c["temps"] = temps
                if not emptyCard:
                    self.cards.append(c)
            i+=1
            if i==37:
                break
        csvf.close()

    
    def create_deck(self):
        self.parse_csv()
        i=1
        self.open_times()
        for card in self.cards:
            self.create_card(card,"card"+str(i).zfill(2)+".png")
            i+=1
        self.cards = []
        self.close_times()

    def gen_planks(self):
        j=0
        xref=80
        yref=100
        deltax=770
        deltay=1070
        plank = Image.new("RGB",(2480,3508),(255,255,255))
        planki = 1
        x = xref
        y = yref
        for i in range(1,37):
            cp = os.path.join(self.workdir,"card"+str(i).zfill(2)+".png")
            if os.path.exists(cp):
                card = Image.open(cp)
                plank.paste(card,(x,y))
                card.close()
                j+=1
                if j<3:
                    x+=deltax
                elif j == 3:
                    x=xref
                    y+=deltay
                elif j<6:
                    x+=deltax
                elif j == 6:
                    x=xref
                    y+=deltay
                elif j<9:
                    x+=deltax
                else:
                    x = xref
                    y = yref
                    plank.save(os.path.join(self.workdir,"planche"+str(planki)+".png"),quality=100)
                    planki+=1
                    j=0
                    plank = Image.new("RGB",(2480,3508),(255,255,255))
        if j!=0:
            plank.save(os.path.join(self.workdir,"planche"+str(planki)+".png"),quality=100)

            
    def create_card(self,card,fname):
        cardimg = self.template.copy()
        drawimg = ImageDraw.Draw(cardimg)
        self.draw_title(drawimg,card["title"])
        self.draw_text(drawimg,card["text"])
        self.draw_times(cardimg,card["temps"])
        cardimg.save(os.path.join(self.workdir,fname),quality=100)
        cardimg.close()

    def draw_title(self,img,title):
        maxlength=580
        xref=365
        yref=73
        size=img.textsize(title,font=self.title_font)
        if size[0] <= maxlength:
            delta=int(size[0]/2)
            img.text((xref-delta,yref),title,(0,0,0),font=self.title_font)
        else:
            size=img.textsize(title,font=self.smaller_title_font)
            delta=int(size[0]/2)
            img.text((xref-delta,yref),title,(0,0,0),font=self.smaller_title_font)

    def draw_text(self,img,text):
        xref=275
        yref=250
        maxlength=407
        biggerlength=0
        size=img.textsize(text,font=self.text_font)
        if size[0] <= maxlength:
            img.text((xref,yref),text,(0,0,0),font=self.text_font)
        else:
            l = text.split()
            ntext=""
            line=""
            for x in l:
                if x == ".N.":
                    linesize=img.textsize(line,font=self.text_font)[0]
                    biggerlength=max(biggerlength,linesize)
                    ntext+="\n"+line.rstrip().lstrip()
                    line=""
                else:
                    s = img.textsize(line+" "+x,font=self.text_font)[0]
                    if s <= maxlength:
                        line+=" "+x
                    else:
                        linesize=img.textsize(line,font=self.text_font)[0]
                        biggerlength=max(biggerlength,linesize)
                        ntext+="\n"+line.rstrip().lstrip()
                        line=x
            if line != "":
                linesize=img.textsize(line,font=self.text_font)[0]
                biggerlength=max(biggerlength,linesize)
                ntext+="\n"+line
            x = xref+int((maxlength-biggerlength)/2)
            img.text((x,yref),ntext.strip(),(0,0,0),font=self.text_font,spacing=8)

    def draw_times(self,img,times):
        y=295
        delta=180
        if "encre" in times.keys():
            for i in range(times["encre"]):
                img.paste(self.encre,(123-self.encre_xdelta,y-self.encre_ydelta),self.encre)
                y+=delta
        if "sang" in times.keys():
            for i in range(times["sang"]):
                img.paste(self.sang,(123-self.sang_xdelta,y-self.sang_ydelta),self.sang)
                y+=delta
        if "seve" in times.keys():
            for i in range(times["seve"]):
                img.paste(self.seve,(123-self.seve_xdelta,y-self.seve_ydelta),self.seve)
                y+=delta
        if "larme" in times.keys():
            for i in range(times["larme"]):
                img.paste(self.larme,(123-self.larme_xdelta,y-self.larme_ydelta),self.larme)
                y+=delta
