#!/usr/bin/env python3

from distutils.core import setup

setup(name="CardMaker",
      version="1.0",
      description="CardMaker, édition de cartes de Ex-Nihilo",
      scripts=["bin/cardmaker"],
      packages=["cardmaker","cardmaker.ui"],
      package_dir={"cardmaker.ui": "cardmaker/ui"},
      package_data = {'cardmaker' : ["resources/*.png","resources/title_font.ttf","resources/text_font.ttf","resources/*.jpg","resources/FONT_LICENSE.txt"] },
      license="special",
)
      
